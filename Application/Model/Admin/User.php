<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/6
 * Time: 13:35
 */

namespace App\Model\Admin;


use App\Model\Model;

class User extends Model
{
    protected $table = 'user_users';

    function add(UserBean $bean)
    {
        return $this->dbConnector()->insert($this->table, $bean->toArray());
    }

    function delete(UserBean $bean)
    {
        return $this->dbConnector()->where('user_id', $bean->getUserId())->delete($this->table);
    }

    function update(UserBean $bean, array $data)
    {
        return $this->dbConnector()->where('user_id', $bean->getUserId())->update($this->table, $data);
    }

    function getAllByXXX(UserBean $bean)
    {
        return $this->dbConnector()->where("user_id", $bean->getUserId())->get($this->table, 20, array("user_id", "user_name"));
    }

    function getById(UserBean $bean)
    {
        return $this->dbConnector()->where('user_id', $bean->getUserId())->getOne($this->table);
    }
}
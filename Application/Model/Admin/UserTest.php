<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/8 0008
 * Time: 下午 05:21
 */

namespace App\Model\Admin;


use App\Model\PoolModel;
use EasySwoole\Core\Component\Logger;
use EasySwoole\Core\Swoole\Coroutine\Client\Mysql;

class UserTest extends PoolModel
{
    protected $table = 'user_users';

    protected $db;

    public function __construct()
    {
        parent::__construct();
        $db = $this->dbConnector();
        if ($db instanceof Mysql || $db instanceof \MysqliDb) {
            $this->db = $db;
        }
    }

    public function __destruct()
    {
        Logger::getInstance()->console(UserTest::class . " obj from pool released!!!");
        $this->releaseDb($this->db);
    }

    function add(UserBean $bean)
    {
        return $this->db->insert($this->table, $bean->toArray());
    }

    function delete(UserBean $bean)
    {
        return $this->db->where('user_id', $bean->getUserId())->delete($this->table);
    }

    function update(UserBean $bean, array $data)
    {
        return $this->db->where('user_id', $bean->getUserId())->update($this->table, $data);
    }

    function getAllByXXX(UserBean $bean)
    {
        return $this->db->where("user_id", $bean->getUserId())->get($this->table, 20, array("user_id", "user_name"));
    }

    function getById(UserBean $bean)
    {
        $res = $this->db->where('user_id', $bean->getUserId())->getOne($this->table, array("user_id", "user_name", "reg_time", "last_login", "mobile_phone", "address", "company", "head_img", "sign", "level_star"));
        $this->releaseDb($this->db);
        Logger::getInstance()->log(var_export($res, true));
        return $res;
    }
}
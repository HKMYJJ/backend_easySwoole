<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/6
 * Time: 13:05
 */

namespace App\Model\Zxz;


use App\Model\Model;

class Comment extends Model
{
    protected $table = 'shop_article_comment';

    function add(CommentBean $bean)
    {
        return $this->dbConnector()->insert($this->table, $bean->toArray());
    }

    function delete(CommentBean $bean)
    {
        return $this->dbConnector()->where('id', $bean->getId())->delete($this->table);
    }

    function update(CommentBean $bean, array $data)
    {
        return $this->dbConnector()->where('id', $bean->getId())->update($this->table, $data);
    }

    function getAllByArticleId(ArticleBean $bean)
    {
        return $this->dbConnector()->where("article_id", $bean->getArticleId())->get($this->table, 20, array("id", "user_id", "article_id", "content", "add_time", "support", "top", "reply_num"));
    }

    function getById(CommentBean $bean)
    {
        return $this->dbConnector()->where('id', $bean->getId())->getOne($this->table);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/8 0008
 * Time: 上午 10:37
 */

namespace App\Model\Zxz;


use EasySwoole\Core\Component\Spl\SplBean;

class ArticleCategoryBean extends SplBean
{
    protected $cat_id;
    protected $cat_name;
    protected $cat_type;
    protected $keywords;
    protected $cat_desc;
    protected $parent_id;
    protected $article_num;
    protected $grade;

    /**
     * @return mixed
     */
    public function getCatId()
    {
        return $this->cat_id;
    }

    /**
     * @param mixed $cat_id
     */
    public function setCatId($cat_id): void
    {
        $this->cat_id = $cat_id;
    }

    /**
     * @return mixed
     */
    public function getCatName()
    {
        return $this->cat_name;
    }

    /**
     * @param mixed $cat_name
     */
    public function setCatName($cat_name): void
    {
        $this->cat_name = $cat_name;
    }

    /**
     * @return mixed
     */
    public function getCatType()
    {
        return $this->cat_type;
    }

    /**
     * @param mixed $cat_type
     */
    public function setCatType($cat_type): void
    {
        $this->cat_type = $cat_type;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param mixed $keywords
     */
    public function setKeywords($keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return mixed
     */
    public function getCatDesc()
    {
        return $this->cat_desc;
    }

    /**
     * @param mixed $cat_desc
     */
    public function setCatDesc($cat_desc): void
    {
        $this->cat_desc = $cat_desc;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param mixed $parent_id
     */
    public function setParentId($parent_id): void
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return mixed
     */
    public function getArticleNum()
    {
        return $this->article_num;
    }

    /**
     * @param mixed $article_num
     */
    public function setArticleNum($article_num): void
    {
        $this->article_num = $article_num;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param mixed $grade
     */
    public function setGrade($grade): void
    {
        $this->grade = $grade;
    }
}
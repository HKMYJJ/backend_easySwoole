<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/4
 * Time: 9:12
 */

namespace App\Model\Zxz;


use EasySwoole\Core\Component\Spl\SplBean;

class ArticleTypeBean extends SplBean
{
    protected $id;
    protected $title;
    protected $t_type;
    protected $pid;
    protected $article_num;
    protected $cat_id;
    protected $grade;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTType()
    {
        return $this->t_type;
    }

    /**
     * @param mixed $t_type
     */
    public function setTType($t_type): void
    {
        $this->t_type = $t_type;
    }

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param mixed $pid
     */
    public function setPid($pid): void
    {
        $this->pid = $pid;
    }

    /**
     * @return mixed
     */
    public function getArticleNum()
    {
        return $this->article_num;
    }

    /**
     * @param mixed $article_num
     */
    public function setArticleNum($article_num): void
    {
        $this->article_num = $article_num;
    }

    /**
     * @return mixed
     */
    public function getCatId()
    {
        return $this->cat_id;
    }

    /**
     * @param mixed $cat_id
     */
    public function setCatId($cat_id): void
    {
        $this->cat_id = $cat_id;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param mixed $grade
     */
    public function setGrade($grade): void
    {
        $this->grade = $grade;
    }

}
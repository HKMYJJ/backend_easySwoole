<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/8 0008
 * Time: 上午 10:36
 */

namespace App\Model\Zxz;


use App\Model\DiModel;
use App\Model\PoolModel;
use EasySwoole\Core\Component\Logger;
use EasySwoole\Core\Swoole\Coroutine\Client\Mysql;

class ArticleCategory extends DiModel
{
    protected $table = 'shop_article_cat';

    protected $db;

    public function __construct()
    {
        parent::__construct();
        $db = $this->dbConnector();
        if ($db instanceof Mysql || $db instanceof \MysqliDb) {
            $this->db = $db;
        }
    }

    public function __destruct()
    {
        Logger::getInstance()->console(ArticleCategory::class . " obj from pool released!!!");
        $this->releaseDb($this->db);
    }

    function add(ArticleCategoryBean $bean)
    {
        return $this->db->insert($this->table, $bean->toArray());
    }

    function delete(ArticleCategoryBean $bean)
    {
        return $this->db->where('cat_id', $bean->getCatId())->delete($this->table);
    }

    function update(ArticleCategoryBean $bean, array $data)
    {
        return $this->db->where('cat_id', $bean->getCatId())->update($this->table, $data);
    }

    function getAllByXXX(ArticleBean $bean)
    {
        return $this->db->where("cat_id", $bean->getCatId())->get($this->table, 20, array("user_id", "user_name"));
    }

    function getById(ArticleBean $bean)
    {
        $res = $this->db->where('cat_id', $bean->getCatId())->getOne($this->table);
        $this->releaseDb($this->db);
        Logger::getInstance()->log(var_export($res, true));
        return $res;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/1
 * Time: 15:27
 */

namespace App\Model\Zxz;


use App\Model\Model;

class Article extends Model
{
    protected $table = 'shop_article';

    function add(ArticleBean $bean)
    {
        return $this->dbConnector()->insert($this->table, $bean->toArray());
    }

    function delete(ArticleBean $bean)
    {
        return $this->dbConnector()->where('article_id', $bean->getArticleId())->delete($this->table);
    }

    function update(ArticleBean $bean, array $data)
    {
        return $this->dbConnector()->where('article_id', $bean->getArticleId())->update($this->table, $data);
    }

    function getAll()
    {
        return $this->dbConnector()->get($this->table, 20, array("a_type","article_id", "cat_id", "title", "author", "source", "flag", "browse_num", "add_time", "article_img", "point"));
    }

    function getById(ArticleBean $bean)
    {
        return $this->dbConnector()->where('article_id', $bean->getArticleId())->getOne($this->table);
    }

    function getByCategory(ArticleBean $bean)
    {
        $list = array();
        try {
            $this->dbConnector()->setTrace(true);
            $this->dbConnector()->join("user_company_comment_select b", "a.type=b.id", "LEFT");
            $this->dbConnector()->join("shop_article_cat c", "a.cat_id=c.cat_id", "LEFT");
            $this->dbConnector()->where("a.cat_id", $bean->getCatId())
                //->where("a.commend_type_phone_cat", 2, "!=")
                ->where("a.is_delete", 0);
            $this->dbConnector()->orderBy("a.commend_type_phone_cat", "Desc");
            $this->dbConnector()->orderBy("a.adopt_time", "Desc");
            $list = $this->dbConnector()->get("shop_article a", 20, array("a.a_type","a.article_id", "a.cat_id", "a.title", "a.author", "a.source", "a.flag", "a.browse_num", "a.add_time", "a.article_img", "a.point", "b.title as type", "c.cat_name", "c.cat_desc"));
            var_dump($this->dbConnector()->trace);
        } catch (\Exception $e) {
            //return $this->dbConnector()->where('cat_id', $bean->getCatId())->get($this->table, 20, array("article_id", "cat_id", "title", "author", "source", "flag", "browse_num", "add_time", "article_img", "point"));
        }
        return $list;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/4
 * Time: 9:13
 */

namespace App\Model\Zxz;


use App\Model\Model;

class ArticleType extends Model
{
    protected $table = 'user_company_comment_select';

    function add(ArticleTypeBean $bean)
    {
        return $this->dbConnector()->insert($this->table, $bean->toArray());
    }

    function delete(ArticleTypeBean $bean)
    {
        return $this->dbConnector()->where('id', $bean->getId())->delete($this->table);
    }

    function update(ArticleTypeBean $bean, array $data)
    {
        return $this->dbConnector()->where('id', $bean->getId())->update($this->table, $data);
    }

    function getAll()
    {
        return $this->dbConnector()->get($this->table, 20);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/6
 * Time: 13:19
 */

namespace App\Model\Zxz;


use App\Model\Model;

class CommentReply extends Model
{
    protected $table = 'shop_article_comment_reply';

    function add(CommentReplyBean $bean)
    {
        return $this->dbConnector()->insert($this->table, $bean->toArray());
    }

    function delete(CommentReplyBean $bean)
    {
        return $this->dbConnector()->where('id', $bean->getId())->delete($this->table);
    }

    function update(CommentReplyBean $bean, array $data)
    {
        return $this->dbConnector()->where('id', $bean->getId())->update($this->table, $data);
    }

    function getAllByCommentId(CommentBean $bean)
    {
        return $this->dbConnector()->where("comment_id", $bean->getId())->get($this->table, 20, array("id", "user_id", "comment_id", "content", "add_time", "support"));
    }

    function getById(CommentReplyBean $bean)
    {
        return $this->dbConnector()->where('id', $bean->getId())->getOne($this->table);
    }
}
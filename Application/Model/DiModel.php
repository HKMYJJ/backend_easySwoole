<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/8 0008
 * Time: 下午 01:56
 */

namespace App\Model;

use EasySwoole\Core\Component\Di;

class DiModel
{
    private $db;

    function __construct()
    {
        $this->db = Di::getInstance()->get("MYSQL");
    }

    function dbConnector()
    {
        return $this->db;
    }

    public function releaseDb($db)
    {
        //do nothing
    }
}
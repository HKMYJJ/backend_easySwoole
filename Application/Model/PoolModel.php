<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/7 0007
 * Time: 下午 02:55
 */

namespace App\Model;

use App\Utility\MysqlPool2;
use EasySwoole\Core\Component\Logger;
use EasySwoole\Core\Component\Pool\PoolManager;
use EasySwoole\Core\Swoole\Coroutine\Client\Mysql;

class PoolModel
{
    private $pool;

    public function __construct()
    {
        $pool = PoolManager::getInstance()->getPool(MysqlPool2::class);
        if ($pool instanceof MysqlPool2) {
            $this->pool = $pool;
        }
    }

    public function dbConnector(): Mysql
    {
        $mysqlObj = $this->pool->getObj();
        if ($mysqlObj instanceof Mysql) {
            // 处理MYSQL 断线重连问题
            if (!$mysqlObj->client()->connected) {
                Logger::getInstance()->log("MySQL connection lost,reconnect....");
                $mysqlObj->connect();
            }
            try {
                $mysqlObj->rawQuery("show databases");
            } catch (\Exception $e) {
                Logger::getInstance()->log("MySQL query failed,reconnect....");
                Logger::getInstance()->logWithTrace(var_export($e, true));
                $mysqlObj->connect();
            }
            return $mysqlObj;
        }
        return null;
    }

    public function releaseDb($db)
    {
        $this->pool->freeObj($db);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/11 0011
 * Time: 上午 09:11
 */

namespace App\Model;

use App\Utility\RedisPool;
use EasySwoole\Core\Component\Pool\PoolManager;
use EasySwoole\Core\Swoole\Coroutine\Client\Redis;

class RedisPoolModel
{
    private $pool;

    public function __construct()
    {
        $pool = PoolManager::getInstance()->getPool(RedisPool::class);
        if ($pool instanceof RedisPool) {
            $this->pool = $pool;
        }
    }

    public function getClient()
    {
        $redisObj = $this->pool->getObj();
        if ($redisObj instanceof Redis) {
            return $redisObj;
        }
        return null;
    }

    public function release($redisObj)
    {
        $this->pool->freeObj($redisObj);
    }

}
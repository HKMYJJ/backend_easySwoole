<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/11 0011
 * Time: 下午 12:58
 */

namespace App\Utility;

use EasySwoole\Core\Utility\Curl\Field;
use EasySwoole\Core\Utility\Curl\Request;
use EasySwoole\Core\Utility\Curl\Response;

class Curl
{
    /**
     * @param string $method
     * @param string $url
     * @param array $params
     * @return Response
     */
    public function request(string $method, string $url, array $params = null): Response
    {
        $request = new Request($url);
        switch ($method) {
            case 'GET' :
                if ($params && isset($params['query'])) {
                    foreach ($params['query'] as $key => $value) {
                        $request->addGet(new Field($key, $value));
                    }
                }
                break;
            case 'POST' :
                if ($params && isset($params['form_params'])) {
                    foreach ($params['form_params'] as $key => $value) {
                        $request->addPost(new Field($key, $value));
                    }
                } elseif ($params && isset($params['body'])) {
                    if (!isset($params['header']['Content-Type'])) {
                        $params['header']['Content-Type'] = 'application/json; charset=utf-8';
                    }
                    $request->setUserOpt([CURLOPT_POSTFIELDS => $params['body']]);
                }
                break;
            default:
                throw new \InvalidArgumentException("method eroor");
                break;
        }
        if (isset($params['header']) && !empty($params['header']) && is_array($params['header'])) {
            foreach ($params['header'] as $key => $value) {
                $string = "{$key}:$value";
                $header[] = $string;
            }
            $request->setUserOpt([CURLOPT_HTTPHEADER => $header]);
        }
        return $request->exec();
    }
}
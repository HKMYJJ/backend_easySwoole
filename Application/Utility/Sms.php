<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/11 0011
 * Time: 下午 12:55
 */

namespace App\Utility;


use EasySwoole\Config;

class Sms
{
    private $params;
    private $config;

    /**
     * Sms constructor.
     */
    public function __construct()
    {
        $config = Config::getInstance()->getConf("sms");
        $params = array();
        $sms_app_key = $config['sms_app_key'];
        $sms_sub_account_id = $config['sms_sub_account_id'];
        $params['app_key'] = $sms_app_key;
        $params['sms_sub_account_id'] = $sms_sub_account_id;
        $params['sms_priority'] = 5; //5：直发； 3：群发
        $params['sms_flag'] = 0;
        $params['sms_template'] = 2;
        $params['sms_isreplay'] = 0; //0:不回复，1:回复
        $this->params = $params;
        $this->config = $config;
    }

    /**
     *
     * 'body' => '{"success":true,"code":0,"messagelog_id":13681171,"msg":"短信发送中.....,请稍后！","task_id":71782176}',
     * 'error' => '',
     * 'errorNo' => 0,
     *
     * @param $mobile
     * @param $code
     * @return \EasySwoole\Core\Utility\Curl\Response
     */
    function send($mobile, $code)
    {
        $sms_app_address = $this->config['sms_app_address'] . "/sms/productSendAction.action";
        $title = '【手机密码验证码短信】';
        $content = '您的验证码：' . $code;
        $content .= "\n这是来自虹口私营企业协会的系统验证信息，您若未提交，或因他人误操所致，深表歉意！";
        $this->params['sms_text'] = base64_encode(urlencode($content));
        $this->params['sms_phone'] = $mobile;
        $sms_app_address .= "?" . http_build_query($this->params);
        $curl = new Curl();
        return $curl->request("GET", $sms_app_address);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/3/6
 * Time: 下午12:14
 */

namespace App\Process;


use EasySwoole\Core\Swoole\Process\AbstractProcess;
use Swoole\Process;

class Test extends AbstractProcess
{

    public function run(Process $process)
    {
        $this->addTick(30000, function () {
//            var_dump('this is '.$this->getProcessName().' process tick');
        });
    }

    public function onShutDown()
    {
        // TODO: Implement onShutDown() method.
    }

    public function onReceive(string $str, ...$args)
    {
        var_dump('process rec' . $str);
    }
}
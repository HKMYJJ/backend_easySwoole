<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/3/6
 * Time: 下午2:53
 */

namespace App\Sock\Parser;

use App\Sock\Controller\Sock;
use EasySwoole\Core\Socket\AbstractInterface\ParserInterface;
use EasySwoole\Core\Socket\Common\CommandBean;

class SockParser implements ParserInterface
{
    private static $controllerMap = array("default" => Sock::class);

    /**
     *
     * {"controller";"xxxx","action":"xxxx","content":"xxxx"}
     *
     * @param $raw
     * @param $client
     * @return CommandBean
     */
    public static function decode($raw, $client)
    {
        $command = new CommandBean();
        $json = json_decode($raw, JSON_OBJECT_AS_ARRAY);
        $command->setControllerClass(self::$controllerMap[$json["controller"] ?? "default"]);
        $command->setAction($json['action'] ?? "hello");
        $command->setArg('content', $json['content'] ?? "NULL");
        return $command;
    }

    public static function encode(string $raw, $client): ?string
    {
        /*
         * 注意，return ''与return null不一样，空字符串一样会回复给客户端，比如在服务端主动心跳测试的场景
         */
        if (strlen($raw) == 0) {
            return null;
        }
        return $raw;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/3/15
 * Time: 下午3:01
 */

namespace App\Sock\Parser;

use App\Sock\Controller\Tcp;
use EasySwoole\Core\Socket\AbstractInterface\ParserInterface;
use EasySwoole\Core\Socket\Common\CommandBean;

class TcpParser implements ParserInterface
{
    private static $controllerMap = array("default" => Tcp::class);

    /*
     * 假定，客户端与服务端都是明文传输。控制格式为 sericeName:actionName:args
     */
    public static function decode($raw, $client)
    {
        $list = explode(":", trim($raw));
        if (count($list) != 3) {
            $list = array("default", "test", "NULL");
        }
        $bean = new CommandBean();
        $controller = array_shift($list);
        $bean->setControllerClass(self::$controllerMap[$controller]);
        $action = array_shift($list);
        $bean->setAction($action);
        $arg = array_shift($list);
        $bean->setArg('args', $arg);
        return $bean;
    }

    public static function encode(string $raw, $client): ?string
    {
        return $raw . "\n";
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/3/6
 * Time: 下午2:54
 */

namespace App\Sock\Controller;


use App\Model\RedisPoolModel;
use EasySwoole\Core\Socket\Response;
use EasySwoole\Core\Socket\AbstractInterface\WebSocketController;
use EasySwoole\Core\Swoole\Coroutine\Client\Redis;
use EasySwoole\Core\Swoole\ServerManager;
use EasySwoole\Core\Swoole\Task\TaskManager;

class Sock extends WebSocketController
{
    /**
     * @var $redisModel RedisPoolModel
     */
    private $redisModel;
    /**
     * @var $redis Redis
     */
    private $redis;

    protected function onRequest(?string $actionName): bool
    {
        $redisModel = new RedisPoolModel();
        $redis = $redisModel->getClient();
        $this->redisModel = $redisModel;
        $this->redis = $redis;
        return parent::onRequest($actionName);
    }

    protected function afterAction($actionName)
    {
        $this->redisModel->release($this->redis);
        parent::afterAction($actionName);
    }

    function actionNotFound(?string $actionName)
    {
        $this->response()->write("action call {$actionName} not found");
    }

    function hello()
    {
        $this->response()->write('call hello with arg:' . $this->request()->getArg('content'));

    }

    public function who()
    {
        var_dump($this->redis->exec("keys", "*"));
        $fd = $this->client()->getFd();
        $this->response()->write('your fd is ' . $fd . ' and detail info is ' . json_encode(ServerManager::getInstance()->getServer()->connection_info($fd)));
    }

    function delay()
    {
        $this->response()->write('this is delay action');
        $client = $this->client();
        //测试异步推送
        TaskManager::async(function () use ($client) {
            sleep(1);
            Response::response($client, 'this is async task res' . time());
        });
    }
}
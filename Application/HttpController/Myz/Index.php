<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/1
 * Time: 15:11
 */

namespace App\HttpController\Myz;


use EasySwoole\Core\Http\AbstractInterface\Controller;
use EasySwoole\Core\Http\Message\Status;

class Index extends Controller
{
    function index()
    {
        $this->writeJson(Status::CODE_OK, "觅鹰站");
    }

}
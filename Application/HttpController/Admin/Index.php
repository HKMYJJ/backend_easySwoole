<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/1
 * Time: 14:58
 */

namespace App\HttpController\Admin;

use App\Model\Admin\PoolUser;
use App\Model\Admin\UserBean;
use App\Model\RedisPoolModel;
use App\Utility\Sms;
use EasySwoole\Core\Component\Logger;
use EasySwoole\Core\Http\AbstractInterface\Controller;
use EasySwoole\Core\Http\Message\Status;
use EasySwoole\Core\Swoole\Coroutine\Client\Redis;

class Index extends Controller
{
    /**
     * @var $redisModel RedisPoolModel
     */
    private $redisModel;
    /**
     * @var $redis Redis
     */
    private $redis;

    protected function onRequest($action): ?bool
    {
        Logger::getInstance()->console("action is " . $action);
        $redisModel = new RedisPoolModel();
        $redis = $redisModel->getClient();
        $this->redisModel = $redisModel;
        $this->redis = $redis;
        return true;
    }

    protected function afterAction($actionName): void
    {
        $this->redisModel->release($this->redis);
        Logger::getInstance()->console("redis obj from pool released!");
    }


    function index()
    {
        $this->writeJson(Status::CODE_OK, "管理后台");
    }

    function userLogin()
    {
        $phone = $this->request()->getRequestParam("phone");
        $password = $this->request()->getRequestParam("password");
        if (is_null($phone) || is_null($password)) {
            $this->writeJson(Status::CODE_FORBIDDEN, null, "param null");
            return;
        }
        $userBean = new UserBean();
        $userBean->setMobilePhone($phone);
        $userModel = new PoolUser();
        $res = $userModel->checkUser($userBean);
        if (is_null($res)) {
            $this->writeJson(Status::CODE_FORBIDDEN, null, "user not exist");
            return;
        }
        $passwd = $res["password"];
        if (md5($password) != $passwd) {
            $this->writeJson(Status::CODE_FORBIDDEN, null, "password not match");
            return;
        }
        $token = $this->getRandomToken();
        $this->redis->exec("hset", "token", $phone, $token);
        $res["token"] = $token;
        $res["phone"] = $phone;
        $this->writeJson(Status::CODE_OK, $res, "OK");
    }

    function sendSmsCode()
    {
        $phone = $this->request()->getRequestParam("phone");
        if (is_null($phone)) {
            $this->writeJson(Status::CODE_FORBIDDEN, null, "param null");
            return;
        }
        $userBean = new UserBean();
        $userBean->setMobilePhone($phone);
        $userModel = new PoolUser();
        $res = $userModel->checkUser($userBean);
        if (is_null($res)) {
            $this->writeJson(Status::CODE_FORBIDDEN, null, "user not exist");
            return;
        }
        $data = array();
        $code = $this->genRandomSmsCode();
        $sms = new Sms();
        $result = $sms->send($phone, $code);
        Logger::getInstance()->logWithTrace(var_export($result, true));
        if ($result && $result->getErrorNo() == 0) {
            $data["result"] = $result->getBody();
        }
        $this->redis->exec("set", "smsCode_" . $phone, $code);
        $this->redis->exec("EXPIRE", "smsCode_" . $phone, 60 * 2);
        $data["code"] = $code;
        $data["phone"] = $phone;
        $this->writeJson(Status::CODE_OK, $data, "OK");
    }

    function smsLogin()
    {
        $phone = $this->request()->getRequestParam("phone");
        $code = $this->request()->getRequestParam("code");
        if (is_null($phone) || is_null($code)) {
            $this->writeJson(Status::CODE_FORBIDDEN, null, "param null");
            return;
        }
        $userBean = new UserBean();
        $userBean->setMobilePhone($phone);
        $userModel = new PoolUser();
        $res = $userModel->checkUser($userBean);
        if (is_null($res)) {
            $this->writeJson(Status::CODE_FORBIDDEN, null, "user not exist");
            return;
        }
        $smsCode = $this->redis->exec("get", "smsCode_" . $phone);
        if ($code != $smsCode) {
            $this->writeJson(Status::CODE_FORBIDDEN, null, "smsCode not match");
            return;
        }
        $data = array();
        $token = $this->getRandomToken();
        $this->redis->exec("hset", "token", $phone, $token);
        $data["token"] = $token;
        $data["phone"] = $phone;
        $this->writeJson(Status::CODE_OK, $data, "OK");
    }

    private function getRandomToken($size = 10)
    {
        return \bin2hex(\random_bytes($size));
    }

    private function genRandomSmsCode()
    {
        return rand(1000, 9999);
    }

}
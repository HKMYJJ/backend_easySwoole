<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/1
 * Time: 15:11
 */

namespace App\HttpController\Zxz;


use App\Model\Admin\PoolUser;
use App\Model\Admin\UserBean;
use App\Model\Zxz\Article;
use App\Model\Zxz\ArticleBean;
use App\Model\Zxz\ArticleCategory;
use App\Model\Zxz\Comment;
use App\Model\Zxz\CommentBean;
use App\Model\Zxz\CommentReply;
use EasySwoole\Core\Component\Logger;
use EasySwoole\Core\Http\AbstractInterface\Controller;
use EasySwoole\Core\Http\Message\Status;

class Index extends Controller
{
    protected function onRequest($action): ?bool
    {
        return parent::onRequest($action);
    }

    function index()
    {
        $this->writeJson(Status::CODE_OK, "资讯站");
    }

    function listAll()
    {
        $model = new Article();
        $list = $model->getAll();
        $this->writeJson(Status::CODE_OK, $list, "OK");
    }

    function listJJYW()
    {
        $bean = new ArticleBean();
        $bean->setCatId("32");
        $model = new Article();
        $list = $model->getByCategory($bean);
        $this->writeJson(Status::CODE_OK, $list, "OK");
    }

    function listMQFC()
    {
        $bean = new ArticleBean();
        $bean->setCatId("33");
        $model = new Article();
        $list = $model->getByCategory($bean);
        $this->writeJson(Status::CODE_OK, $list, "OK");
    }

    private function getUserById($uid)
    {
        $bean = new UserBean();
        $bean->setUserId($uid);
        $model = new PoolUser();
        return $model->getById($bean);
    }

    private function getCategoryById($cid)
    {
        $bean = new ArticleBean();
        $bean->setCatId($cid);
        $model = new ArticleCategory();
        $category = $model->getById($bean);
        return is_null($category) ? array("cat_name" => "", "cat_desc" => "", "keywords" => "") : $category;
    }

    function listOneComment()
    {
        $id = $this->request()->getRequestParam("id");
        if ($id) {
            $commentBean = new CommentBean();
            $commentBean->setId($id);
            $commentModel = new Comment();
            $commentObj = $commentModel->getById($commentBean);
            $commentObj["userInfo"] = $this->getUserById($commentObj["user_id"]);
            $replyModel = new CommentReply();
            $replyList = $replyModel->getAllByCommentId($commentBean);
            foreach ($replyList as $i => $reply) {
                $replyList[$i]["userInfo"] = $this->getUserById($reply["user_id"]);
            }
            $commentObj["reply"] = $replyList;
            $this->writeJson(Status::CODE_OK, $commentObj, "OK");
            return;
        }
        $this->writeJson(Status::CODE_NOT_FOUND, "", "not found");
    }

    function listOne()
    {
        $id = $this->request()->getRequestParam("id");
        if ($id) {
            $bean = new ArticleBean();
            $bean->setArticleId($id);
            $model = new Article();
            $one = $model->getById($bean);
            if (mb_strlen($one["content"]) > 1000) {
                $one["content"] = mb_substr($one["content"], 0, 200);
            }
            $commentModel = new Comment();
            $commentList = $commentModel->getAllByArticleId($bean);
            foreach ($commentList as $i => $comment) {
                $commentBean = new CommentBean();
                $commentBean->setId($comment["id"]);
                $replyModel = new CommentReply();
                $replyList = $replyModel->getAllByCommentId($commentBean);
                $commentList[$i]["userInfo"] = $this->getUserById($comment["user_id"]);
                foreach ($replyList as $ii => $reply) {
                    $replyList[$ii]["userInfo"] = $this->getUserById($reply["user_id"]);
                }
                $commentList[$i]["reply"] = $replyList;
            }
            $one["comment"] = $commentList;
            $one["category"] = $this->getCategoryById($one["cat_id"]);
            Logger::getInstance()->log(var_export($one, true));
            $this->writeJson(Status::CODE_OK, $one, "OK");
            return;
        }
        $this->writeJson(Status::CODE_NOT_FOUND, "", "not found");
    }

}